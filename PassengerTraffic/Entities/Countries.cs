namespace PassengerTraffic
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Countries
    {
        public Countries()
        {
            Cities = new HashSet<Cities>();
        }

        [Key]
        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public virtual ICollection<Cities> Cities { get; set; }
    }
}
