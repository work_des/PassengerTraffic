namespace PassengerTraffic
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class TypesTickets
    {
        public TypesTickets()
        {
            Tickets = new HashSet<Tickets>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string NameType { get; set; }

        public virtual ICollection<Tickets> Tickets { get; set; }
    }
}
