namespace PassengerTraffic
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Transports
    {
        public Transports()
        {
            Directions = new HashSet<Directions>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string NameAuto { get; set; }

        [Required]
        [StringLength(10)]
        public string GosNumber { get; set; }

        public int NumberOfSeats { get; set; }

        public virtual ICollection<Directions> Directions { get; set; }
    }
}
