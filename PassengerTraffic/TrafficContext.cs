namespace PassengerTraffic
{
    using System.Data.Entity;

    // �������� ��
    public class TrafficContext : DbContext
    {
        public TrafficContext() : base("name=TrafficContext") { }

        public virtual DbSet<Cities> Cities { get; set; }
        public virtual DbSet<Countries> Countries { get; set; }
        public virtual DbSet<Directions> Directions { get; set; }
        public virtual DbSet<SellTickets> SellTickets { get; set; }
        public virtual DbSet<Tickets> Tickets { get; set; }
        public virtual DbSet<Transports> Transports { get; set; }
        public virtual DbSet<TypesTickets> TypesTickets { get; set; }

        // �������� �� �� ������
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cities>()
                .HasMany(e => e.Directions)
                .WithRequired(e => e.Cities)
                .HasForeignKey(e => e.Arrival)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cities>()
                .HasMany(e => e.Directions1)
                .WithRequired(e => e.Cities1)
                .HasForeignKey(e => e.Departure)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Countries>()
                .HasMany(e => e.Cities)
                .WithRequired(e => e.Countries)
                .HasForeignKey(e => e.IdCountry)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Directions>()
                .HasMany(e => e.Tickets)
                .WithRequired(e => e.Directions)
                .HasForeignKey(e => e.IdDirection)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Transports>()
                .HasMany(e => e.Directions)
                .WithRequired(e => e.Transports)
                .HasForeignKey(e => e.IdTransport)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TypesTickets>()
                .HasMany(e => e.Tickets)
                .WithRequired(e => e.TypesTickets)
                .HasForeignKey(e => e.IdTypeTicket)
                .WillCascadeOnDelete(false);
        }
    }
}
