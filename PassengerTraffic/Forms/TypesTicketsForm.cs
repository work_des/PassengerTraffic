﻿using System;
using System.Data.Entity;
using System.Windows.Forms;

namespace PassengerTraffic.Forms
{
    public partial class TypesTicketsForm : Form
    {
        private readonly TrafficContext ctx = new TrafficContext();
        private readonly BindingSource bs = new BindingSource();

        public TypesTicketsForm()
        {
            InitializeComponent();
            gridViewTypesTickets.AutoGenerateColumns = false;

            ctx.TypesTickets.Load();
            bs.DataSource = ctx.TypesTickets.Local.ToBindingList();
            gridViewTypesTickets.DataSource = bs;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            gridViewTypesTickets.EndEdit();
            bs.EndEdit();
            ctx.SaveChanges();
            Close();
        }
    }
}
