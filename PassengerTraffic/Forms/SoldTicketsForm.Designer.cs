﻿namespace PassengerTraffic.Forms
{
    partial class SoldTicketsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridSoldЕickets = new System.Windows.Forms.DataGridView();
            this.btnGetSoldTickets = new System.Windows.Forms.Button();
            this.dtSatart = new System.Windows.Forms.DateTimePicker();
            this.dtEnd = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.NumberСruise = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateSale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Summ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TypeTicket = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DepartureCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DepartureCity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ArrivalCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ArrivalCity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateDeparture = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateArrival = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Transport = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GosNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridSoldЕickets)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridSoldЕickets
            // 
            this.dataGridSoldЕickets.AllowUserToAddRows = false;
            this.dataGridSoldЕickets.AllowUserToDeleteRows = false;
            this.dataGridSoldЕickets.AllowUserToOrderColumns = true;
            this.dataGridSoldЕickets.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridSoldЕickets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridSoldЕickets.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NumberСruise,
            this.DateSale,
            this.Summ,
            this.TypeTicket,
            this.DepartureCountry,
            this.DepartureCity,
            this.ArrivalCountry,
            this.ArrivalCity,
            this.DateDeparture,
            this.DateArrival,
            this.Transport,
            this.GosNumber});
            this.dataGridSoldЕickets.Location = new System.Drawing.Point(0, 34);
            this.dataGridSoldЕickets.Name = "dataGridSoldЕickets";
            this.dataGridSoldЕickets.Size = new System.Drawing.Size(1244, 384);
            this.dataGridSoldЕickets.TabIndex = 0;
            // 
            // btnGetSoldTickets
            // 
            this.btnGetSoldTickets.Location = new System.Drawing.Point(306, 9);
            this.btnGetSoldTickets.Name = "btnGetSoldTickets";
            this.btnGetSoldTickets.Size = new System.Drawing.Size(75, 23);
            this.btnGetSoldTickets.TabIndex = 1;
            this.btnGetSoldTickets.Text = "Найти";
            this.btnGetSoldTickets.UseVisualStyleBackColor = true;
            this.btnGetSoldTickets.Click += new System.EventHandler(this.btnGetSoldTickets_Click);
            // 
            // dtSatart
            // 
            this.dtSatart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtSatart.Location = new System.Drawing.Point(35, 12);
            this.dtSatart.Name = "dtSatart";
            this.dtSatart.Size = new System.Drawing.Size(93, 20);
            this.dtSatart.TabIndex = 2;
            // 
            // dtEnd
            // 
            this.dtEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtEnd.Location = new System.Drawing.Point(182, 12);
            this.dtEnd.Name = "dtEnd";
            this.dtEnd.Size = new System.Drawing.Size(93, 20);
            this.dtEnd.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "C:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(152, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "По:";
            // 
            // NumberСruise
            // 
            this.NumberСruise.DataPropertyName = "NumberСruise";
            this.NumberСruise.HeaderText = "Номер рейса";
            this.NumberСruise.Name = "NumberСruise";
            // 
            // DateSale
            // 
            this.DateSale.DataPropertyName = "DateSale";
            this.DateSale.HeaderText = "Дата продажи";
            this.DateSale.Name = "DateSale";
            // 
            // Summ
            // 
            this.Summ.DataPropertyName = "Summ";
            this.Summ.HeaderText = "Цена";
            this.Summ.Name = "Summ";
            // 
            // TypeTicket
            // 
            this.TypeTicket.DataPropertyName = "TypeTicket";
            this.TypeTicket.HeaderText = "Тип билета";
            this.TypeTicket.Name = "TypeTicket";
            // 
            // DepartureCountry
            // 
            this.DepartureCountry.DataPropertyName = "DepartureCountry";
            this.DepartureCountry.HeaderText = "Страна отпр.";
            this.DepartureCountry.Name = "DepartureCountry";
            // 
            // DepartureCity
            // 
            this.DepartureCity.DataPropertyName = "DepartureCity";
            this.DepartureCity.HeaderText = "Город отпр.";
            this.DepartureCity.Name = "DepartureCity";
            // 
            // ArrivalCountry
            // 
            this.ArrivalCountry.DataPropertyName = "ArrivalCountry";
            this.ArrivalCountry.HeaderText = "Страна приб.";
            this.ArrivalCountry.Name = "ArrivalCountry";
            // 
            // ArrivalCity
            // 
            this.ArrivalCity.DataPropertyName = "ArrivalCity";
            this.ArrivalCity.HeaderText = "Город приб.";
            this.ArrivalCity.Name = "ArrivalCity";
            // 
            // DateDeparture
            // 
            this.DateDeparture.DataPropertyName = "DateDeparture";
            this.DateDeparture.HeaderText = "Дата отпр.";
            this.DateDeparture.Name = "DateDeparture";
            // 
            // DateArrival
            // 
            this.DateArrival.DataPropertyName = "DateArrival";
            this.DateArrival.HeaderText = "Дата приб.";
            this.DateArrival.Name = "DateArrival";
            // 
            // Transport
            // 
            this.Transport.DataPropertyName = "Transport";
            this.Transport.HeaderText = "Транспорт";
            this.Transport.Name = "Transport";
            // 
            // GosNumber
            // 
            this.GosNumber.DataPropertyName = "GosNumber";
            this.GosNumber.HeaderText = "Гос номер";
            this.GosNumber.Name = "GosNumber";
            // 
            // SoldTicketsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1244, 418);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtEnd);
            this.Controls.Add(this.dtSatart);
            this.Controls.Add(this.btnGetSoldTickets);
            this.Controls.Add(this.dataGridSoldЕickets);
            this.Name = "SoldTicketsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Проданные билеты";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridSoldЕickets)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridSoldЕickets;
        private System.Windows.Forms.Button btnGetSoldTickets;
        private System.Windows.Forms.DateTimePicker dtSatart;
        private System.Windows.Forms.DateTimePicker dtEnd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumberСruise;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateSale;
        private System.Windows.Forms.DataGridViewTextBoxColumn Summ;
        private System.Windows.Forms.DataGridViewTextBoxColumn TypeTicket;
        private System.Windows.Forms.DataGridViewTextBoxColumn DepartureCountry;
        private System.Windows.Forms.DataGridViewTextBoxColumn DepartureCity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArrivalCountry;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArrivalCity;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateDeparture;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateArrival;
        private System.Windows.Forms.DataGridViewTextBoxColumn Transport;
        private System.Windows.Forms.DataGridViewTextBoxColumn GosNumber;
    }
}