﻿using PassengerTraffic.Forms;
using System;
using System.Linq;
using System.Windows.Forms;

namespace PassengerTraffic
{
    public partial class MainForm : Form
    {
        TrafficContext ctx = new TrafficContext();
        BindingSource bs = new BindingSource();

        public MainForm()
        {
            InitializeComponent();
            gridViewDirections.AutoGenerateColumns = false;
            gridViewDirections.DataSource = bs;

            bs.DataSource = ctx.Directions.ToList();

            bs.CurrentChanged += (s, e) =>
            {
                int? id = ((Directions)bs.Current)?.Id;

                cbTypeTicket.DataSource = ctx.Tickets.Where(x => x.IdDirection == id).ToList();
                cbTypeTicket.DisplayMember = "TypeTicketStr";
            };

            lbDate.DataBindings.Add(new Binding("Text", bs, "DateDepartureStr"));
            lbFrom.DataBindings.Add(new Binding("Text", bs, "DepartureStr"));
            lbTo.DataBindings.Add(new Binding("Text", bs, "ArrivalStr"));
            RefreshLb();
        }

        #region MENU
        private void menuItemTransport_Click(object sender, EventArgs e)
        {
            var f = new TransportForm();
            f.ShowDialog();
        }

        private void menuItemCities_Click(object sender, EventArgs e)
        {
            var f = new CitiesForm();
            f.ShowDialog();
        }

        private void menuItemTypesTickets_Click(object sender, EventArgs e)
        {
            var f = new TypesTicketsForm();
            f.ShowDialog();
        }

        private void menuItemReports_Click(object sender, EventArgs e)
        {
            var f = new SoldTicketsForm();
            f.ShowDialog();
        }

        #endregion

        //Поиск рейсов
        private void btnSearch_Click(object sender, EventArgs e)
        {  
            bs.DataSource = ctx.Directions.Where(x => 
            (x.Cities1.Name.Contains(tbCities.Text) ||
            x.Cities1.Countries.Name.Contains(tbCities.Text)) &&
            x.DateDeparture >= timePickerFilter.Value.Date)
            .ToList();
        }

        //Сброс фильтра
        private void btnReset_Click(object sender, EventArgs e) => RefreshAll();

        //Продажа билетов
        private void btnSell_Click(object sender, EventArgs e)
        {
            var direction = bs.Current as Directions;

            if (direction == null)
                throw new Exception("Необходимо создать направление");
            if (direction.CountAvailable == 0)
                throw new Exception("Билеты на данный рейс закончились");
            if (direction.DateDeparture <= DateTime.Now)
                throw new Exception("Продажа невозможна. Данное направление уже закрыто");

            ctx.SellTickets.Add(CreateSellTicket(direction));
            direction.CountAvailable -= 1;

            ctx.SaveChanges();
            RefreshLb();
        }

        //Возврат билетов
        private void btnReturn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Сделать возврат билета данного направления?", "Возврат билета", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var direction = bs.Current as Directions;
                if (direction.DateDeparture > DateTime.Now)
                {
                    var sellTicket = CreateSellTicket(direction);
                    var sellTicketRemove = ctx.SellTickets.Where(x =>
                       x.NumberСruise == sellTicket.NumberСruise &&
                       x.ArrivalCity == sellTicket.ArrivalCity &&
                       x.DepartureCity == sellTicket.DepartureCity &&
                       x.DateArrival == sellTicket.DateArrival &&
                       x.DateDeparture == sellTicket.DateDeparture &&
                       x.TypeTicket == sellTicket.TypeTicket).FirstOrDefault();

                    if (sellTicketRemove == null)
                        throw new Exception("В данном направлении нет проданных билетов");

                    ctx.SellTickets.Remove(sellTicketRemove);
                    direction.CountAvailable += 1;

                    ctx.SaveChanges();
                    RefreshLb();
                }
                else
                    throw new Exception("Возврат невозможен. Данное направление уже закрыто");
            }
        }

        //Создает объект проданного билета из данных направления
        private SellTickets CreateSellTicket(Directions direction)
        {
            return new SellTickets
            {
                NumberСruise = direction.NumberСruise,
                DateSale = DateTime.Now,
                TypeTicket = direction.Tickets.Single(x => x.Id == (int)cbTypeTicket.SelectedValue).TypeTicketStr,
                Summ = direction.Tickets.Single(x => x.Id == (int)cbTypeTicket.SelectedValue).Price,
                DepartureCountry = direction.Cities.Countries.Name,
                DepartureCity = direction.Cities.Name,
                ArrivalCountry = direction.Cities1.Countries.Name,
                ArrivalCity = direction.Cities1.Name,
                DateDeparture = direction.DateDeparture,
                DateArrival = direction.DateArrival,
                Transport = direction.Transports.NameAuto,
                GosNumber = direction.Transports.GosNumber
            };
        }

        //Новое направление
        private void btnAdd_Click(object sender, EventArgs e)
        {
            var f = new EditDirectionsForm(0);
            f.ShowDialog();
            RefreshAll();
        }

        //Двойной клик по выбранной строке грида (редактирование направления)
        private void gridViewDirections_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var f = new EditDirectionsForm(((Directions)bs.Current).Id);
            f.ShowDialog();
            RefreshAll();
        }

        //Удаление направлений
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы действительно хотите удалить это направление?", "Удаление", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ctx.Directions.Remove((Directions)bs.Current);
                ctx.SaveChanges();
                RefreshAll();
            }
        }

        //Обновление грида
        private void RefreshAll()
        {
            foreach (var entity in ctx.ChangeTracker.Entries())
                entity.Reload();

            bs.DataSource = ctx.Directions.ToList();
            bs.ResetBindings(false);
            gridViewDirections.Update();
        }

        //Обновление индикаторов (Сумма выручки и кол-во пасажиров за текущий день)
        private void RefreshLb()
        {
            lbSummToday.Text = ctx.SellTickets.ToList().Where(x => x.DateSale.Date == DateTime.Now.Date).Sum(s => s.Summ).ToString();
            lbCountToday.Text = ctx.SellTickets.ToList().Count(x => x.DateSale.Date == DateTime.Now.Date).ToString();
            bs.ResetBindings(false);
        }
    }
}
