﻿namespace PassengerTraffic
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menu = new System.Windows.Forms.MenuStrip();
            this.menuItemTransport = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemCities = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemTypesTickets = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemReports = new System.Windows.Forms.ToolStripMenuItem();
            this.gridViewDirections = new System.Windows.Forms.DataGridView();
            this.NumberСruise = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Departure = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Arrival = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateDeparture = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateArrival = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CountAvailable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdTransport = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSell = new System.Windows.Forms.Button();
            this.cbTypeTicket = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lbDate = new System.Windows.Forms.Label();
            this.lbTo = new System.Windows.Forms.Label();
            this.lbFrom = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnReturn = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.timePickerFilter = new System.Windows.Forms.DateTimePicker();
            this.tbCities = new System.Windows.Forms.TextBox();
            this.btnFilter = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lbSummToday = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbCountToday = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.menu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDirections)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemTransport,
            this.menuItemCities,
            this.menuItemTypesTickets,
            this.menuItemReports});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(961, 24);
            this.menu.TabIndex = 0;
            this.menu.Text = "menu";
            // 
            // menuItemTransport
            // 
            this.menuItemTransport.Name = "menuItemTransport";
            this.menuItemTransport.Size = new System.Drawing.Size(78, 20);
            this.menuItemTransport.Text = "Транспорт";
            this.menuItemTransport.Click += new System.EventHandler(this.menuItemTransport_Click);
            // 
            // menuItemCities
            // 
            this.menuItemCities.Name = "menuItemCities";
            this.menuItemCities.Size = new System.Drawing.Size(58, 20);
            this.menuItemCities.Text = "Города";
            this.menuItemCities.Click += new System.EventHandler(this.menuItemCities_Click);
            // 
            // menuItemTypesTickets
            // 
            this.menuItemTypesTickets.Name = "menuItemTypesTickets";
            this.menuItemTypesTickets.Size = new System.Drawing.Size(97, 20);
            this.menuItemTypesTickets.Text = "Типы билетов";
            this.menuItemTypesTickets.Click += new System.EventHandler(this.menuItemTypesTickets_Click);
            // 
            // menuItemReports
            // 
            this.menuItemReports.Name = "menuItemReports";
            this.menuItemReports.Size = new System.Drawing.Size(127, 20);
            this.menuItemReports.Text = "Проданные билеты";
            this.menuItemReports.Click += new System.EventHandler(this.menuItemReports_Click);
            // 
            // gridViewDirections
            // 
            this.gridViewDirections.AllowUserToAddRows = false;
            this.gridViewDirections.AllowUserToDeleteRows = false;
            this.gridViewDirections.AllowUserToOrderColumns = true;
            this.gridViewDirections.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridViewDirections.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridViewDirections.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NumberСruise,
            this.Departure,
            this.Arrival,
            this.DateDeparture,
            this.DateArrival,
            this.CountAvailable,
            this.IdTransport});
            this.gridViewDirections.Location = new System.Drawing.Point(6, 92);
            this.gridViewDirections.MultiSelect = false;
            this.gridViewDirections.Name = "gridViewDirections";
            this.gridViewDirections.ReadOnly = true;
            this.gridViewDirections.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridViewDirections.Size = new System.Drawing.Size(925, 245);
            this.gridViewDirections.TabIndex = 1;
            this.gridViewDirections.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridViewDirections_CellDoubleClick);
            // 
            // NumberСruise
            // 
            this.NumberСruise.DataPropertyName = "NumberСruise";
            this.NumberСruise.HeaderText = "Номер рейса";
            this.NumberСruise.Name = "NumberСruise";
            this.NumberСruise.ReadOnly = true;
            // 
            // Departure
            // 
            this.Departure.DataPropertyName = "DepartureStr";
            this.Departure.HeaderText = "От куда";
            this.Departure.Name = "Departure";
            this.Departure.ReadOnly = true;
            this.Departure.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Departure.Width = 150;
            // 
            // Arrival
            // 
            this.Arrival.DataPropertyName = "ArrivalStr";
            this.Arrival.HeaderText = "Куда";
            this.Arrival.Name = "Arrival";
            this.Arrival.ReadOnly = true;
            this.Arrival.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Arrival.Width = 150;
            // 
            // DateDeparture
            // 
            this.DateDeparture.DataPropertyName = "DateDepartureStr";
            this.DateDeparture.HeaderText = "Дата отправления";
            this.DateDeparture.Name = "DateDeparture";
            this.DateDeparture.ReadOnly = true;
            this.DateDeparture.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DateDeparture.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // DateArrival
            // 
            this.DateArrival.DataPropertyName = "DateArrivalStr";
            this.DateArrival.HeaderText = "Дата прибытия";
            this.DateArrival.Name = "DateArrival";
            this.DateArrival.ReadOnly = true;
            this.DateArrival.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DateArrival.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CountAvailable
            // 
            this.CountAvailable.DataPropertyName = "CountAvailable";
            this.CountAvailable.HeaderText = "Кол-во свободных мест";
            this.CountAvailable.Name = "CountAvailable";
            this.CountAvailable.ReadOnly = true;
            this.CountAvailable.Width = 80;
            // 
            // IdTransport
            // 
            this.IdTransport.DataPropertyName = "TransportStr";
            this.IdTransport.HeaderText = "Транспорт";
            this.IdTransport.Name = "IdTransport";
            this.IdTransport.ReadOnly = true;
            this.IdTransport.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IdTransport.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.IdTransport.Width = 200;
            // 
            // btnSell
            // 
            this.btnSell.Location = new System.Drawing.Point(426, 16);
            this.btnSell.Name = "btnSell";
            this.btnSell.Size = new System.Drawing.Size(75, 23);
            this.btnSell.TabIndex = 2;
            this.btnSell.Text = "Продать";
            this.btnSell.UseVisualStyleBackColor = true;
            this.btnSell.Click += new System.EventHandler(this.btnSell_Click);
            // 
            // cbTypeTicket
            // 
            this.cbTypeTicket.DisplayMember = "TypeTicketStr";
            this.cbTypeTicket.FormattingEnabled = true;
            this.cbTypeTicket.Location = new System.Drawing.Point(278, 16);
            this.cbTypeTicket.Name = "cbTypeTicket";
            this.cbTypeTicket.Size = new System.Drawing.Size(121, 21);
            this.cbTypeTicket.TabIndex = 3;
            this.cbTypeTicket.ValueMember = "Id";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lbDate);
            this.groupBox1.Controls.Add(this.lbTo);
            this.groupBox1.Controls.Add(this.lbFrom);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.btnReturn);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.btnSell);
            this.groupBox1.Controls.Add(this.cbTypeTicket);
            this.groupBox1.Location = new System.Drawing.Point(12, 376);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(937, 88);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Продажа билетов";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(205, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Тип билета:";
            // 
            // lbDate
            // 
            this.lbDate.AutoSize = true;
            this.lbDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbDate.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbDate.Location = new System.Drawing.Point(68, 61);
            this.lbDate.Name = "lbDate";
            this.lbDate.Size = new System.Drawing.Size(11, 13);
            this.lbDate.TabIndex = 20;
            this.lbDate.Text = "*";
            // 
            // lbTo
            // 
            this.lbTo.AutoSize = true;
            this.lbTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbTo.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbTo.Location = new System.Drawing.Point(68, 40);
            this.lbTo.Name = "lbTo";
            this.lbTo.Size = new System.Drawing.Size(11, 13);
            this.lbTo.TabIndex = 19;
            this.lbTo.Text = "*";
            // 
            // lbFrom
            // 
            this.lbFrom.AutoSize = true;
            this.lbFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbFrom.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbFrom.Location = new System.Drawing.Point(68, 19);
            this.lbFrom.Name = "lbFrom";
            this.lbFrom.Size = new System.Drawing.Size(11, 13);
            this.lbFrom.TabIndex = 18;
            this.lbFrom.Text = "*";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 61);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Дата отп.:";
            // 
            // btnReturn
            // 
            this.btnReturn.Location = new System.Drawing.Point(426, 59);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(75, 23);
            this.btnReturn.TabIndex = 16;
            this.btnReturn.Text = "Возврат";
            this.btnReturn.UseVisualStyleBackColor = true;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(28, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Куда:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "От куда:";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.btnDelete);
            this.groupBox2.Controls.Add(this.btnAdd);
            this.groupBox2.Controls.Add(this.btnReset);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.timePickerFilter);
            this.groupBox2.Controls.Add(this.tbCities);
            this.groupBox2.Controls.Add(this.btnFilter);
            this.groupBox2.Controls.Add(this.gridViewDirections);
            this.groupBox2.Location = new System.Drawing.Point(12, 27);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(937, 343);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Направления";
            // 
            // btnDelete
            // 
            this.btnDelete.Image = global::PassengerTraffic.Properties.Resources.del;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(103, 60);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 26);
            this.btnDelete.TabIndex = 13;
            this.btnDelete.Text = "Удалить";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Image = global::PassengerTraffic.Properties.Resources.add;
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(10, 60);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(81, 26);
            this.btnAdd.TabIndex = 12;
            this.btnAdd.Text = "Добавить";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnReset
            // 
            this.btnReset.Image = global::PassengerTraffic.Properties.Resources.clear;
            this.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReset.Location = new System.Drawing.Point(507, 18);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(78, 26);
            this.btnReset.TabIndex = 11;
            this.btnReset.Text = "Сбросить";
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(185, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Дата отправки >=";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Куда:";
            // 
            // timePickerFilter
            // 
            this.timePickerFilter.CustomFormat = "dd.MM.yyyy";
            this.timePickerFilter.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timePickerFilter.Location = new System.Drawing.Point(286, 22);
            this.timePickerFilter.Name = "timePickerFilter";
            this.timePickerFilter.Size = new System.Drawing.Size(113, 20);
            this.timePickerFilter.TabIndex = 8;
            // 
            // tbCities
            // 
            this.tbCities.Location = new System.Drawing.Point(43, 22);
            this.tbCities.Name = "tbCities";
            this.tbCities.Size = new System.Drawing.Size(113, 20);
            this.tbCities.TabIndex = 7;
            // 
            // btnFilter
            // 
            this.btnFilter.Image = global::PassengerTraffic.Properties.Resources.filter;
            this.btnFilter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFilter.Location = new System.Drawing.Point(417, 18);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(75, 26);
            this.btnFilter.TabIndex = 6;
            this.btnFilter.Text = "Фильтр";
            this.btnFilter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbSummToday);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.lbCountToday);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox3.Location = new System.Drawing.Point(0, 470);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(961, 43);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            // 
            // lbSummToday
            // 
            this.lbSummToday.AutoSize = true;
            this.lbSummToday.ForeColor = System.Drawing.Color.Red;
            this.lbSummToday.Location = new System.Drawing.Point(286, 21);
            this.lbSummToday.Name = "lbSummToday";
            this.lbSummToday.Size = new System.Drawing.Size(13, 13);
            this.lbSummToday.TabIndex = 3;
            this.lbSummToday.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label3.Location = new System.Drawing.Point(217, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 26);
            this.label3.TabIndex = 2;
            this.label3.Text = "Выручка\r\nза сегодня";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbCountToday
            // 
            this.lbCountToday.AutoSize = true;
            this.lbCountToday.ForeColor = System.Drawing.Color.Red;
            this.lbCountToday.Location = new System.Drawing.Point(62, 21);
            this.lbCountToday.Name = "lbCountToday";
            this.lbCountToday.Size = new System.Drawing.Size(13, 13);
            this.lbCountToday.TabIndex = 1;
            this.lbCountToday.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label1.Location = new System.Drawing.Point(3, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Поток за\r\nсегодня";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(164, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "И";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(961, 513);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menu);
            this.MainMenuStrip = this.menu;
            this.MinimumSize = new System.Drawing.Size(937, 552);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Учет пасажиропотока";
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDirections)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem menuItemTransport;
        private System.Windows.Forms.ToolStripMenuItem menuItemTypesTickets;
        private System.Windows.Forms.ToolStripMenuItem menuItemReports;
        private System.Windows.Forms.DataGridView gridViewDirections;
        private System.Windows.Forms.Button btnSell;
        private System.Windows.Forms.ComboBox cbTypeTicket;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lbSummToday;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbCountToday;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem menuItemCities;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.DateTimePicker timePickerFilter;
        private System.Windows.Forms.TextBox tbCities;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnReturn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbDate;
        private System.Windows.Forms.Label lbTo;
        private System.Windows.Forms.Label lbFrom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumberСruise;
        private System.Windows.Forms.DataGridViewTextBoxColumn Departure;
        private System.Windows.Forms.DataGridViewTextBoxColumn Arrival;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateDeparture;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateArrival;
        private System.Windows.Forms.DataGridViewTextBoxColumn CountAvailable;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdTransport;
        private System.Windows.Forms.Label label4;
    }
}

