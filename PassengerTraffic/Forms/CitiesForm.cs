﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Windows.Forms;

namespace PassengerTraffic.Forms
{
    public partial class CitiesForm : Form
    {
        private readonly TrafficContext ctx = new TrafficContext();
        private TrafficContext ctxCurrent;
        private readonly BindingSource bs = new BindingSource();
        private int idCoutry = 0;

        public CitiesForm()
        {
            InitializeComponent();

            bs.CurrentChanged += Bs_CurrentChanged;
            gridViewCountries.DataSource = bs;

            gridViewCountries.AutoGenerateColumns = false;
            gridViewCities.AutoGenerateColumns = false;

            ctx.Countries.Load();

            bs.DataSource = ctx.Countries.Local.ToBindingList();
        }

        private void Bs_CurrentChanged(object sender, EventArgs e)
        {
            ctxCurrent = null;
            ctxCurrent = new TrafficContext();

            if (bs.Current != null)
                idCoutry = (bs.Current as Countries).Id;

            ctxCurrent.Cities.Where(x => x.IdCountry == idCoutry).Load();

            gridViewCities.DataSource = ctxCurrent.Cities.Local.ToBindingList();
            gridViewCities.Update();
        }

        //Значение по умолчанию при добавлении строки
        private void gridViewCities_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e) => e.Row.Cells["IdCountry"].Value = idCoutry;


        private void gridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Save();

            if (e.RowIndex != -1)
                idCoutry = (int)gridViewCountries.CurrentRow.Cells["Id"].Value;
        }

        private void Save()
        {
            gridViewCountries.EndEdit();
            gridViewCities.EndEdit();
            bs.EndEdit();
            ctx.SaveChanges();
            ctxCurrent?.SaveChanges();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Save();
            Close();
        }
    }
}
