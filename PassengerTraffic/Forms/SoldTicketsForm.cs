﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace PassengerTraffic.Forms
{
    public partial class SoldTicketsForm : Form
    {
        public SoldTicketsForm()
        {
            InitializeComponent();
            dataGridSoldЕickets.AutoGenerateColumns = false;
        }

        private void btnGetSoldTickets_Click(object sender, EventArgs e)
        {
            var start = dtSatart.Value.Date;
            var end = new DateTime(dtEnd.Value.Year, dtEnd.Value.Month, dtEnd.Value.Day, 23, 59, 59);

            var ctx = new TrafficContext();
            dataGridSoldЕickets.DataSource = ctx.SellTickets.Where(x => x.DateSale >= start && x.DateSale <= end).ToList();
        }
    }
}
