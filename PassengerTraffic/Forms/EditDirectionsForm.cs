﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Windows.Forms;

namespace PassengerTraffic.Forms
{
    public partial class EditDirectionsForm : Form
    {
        private readonly int idDirection = 0;
        readonly TrafficContext ctx = new TrafficContext();
        Directions d;

        public EditDirectionsForm(int idDirection)
        {
            InitializeComponent();
            gridViewTickets.AutoGenerateColumns = false;

            this.idDirection = idDirection;

            cbTransports.DataSource = ctx.Transports.ToList();
            cbCitiesFrom.DataSource = ctx.Cities.ToList();
            cbCitiesTo.DataSource = ctx.Cities.ToList();

            var dataColumn = (DataGridViewComboBoxColumn)gridViewTickets.Columns["IdTypeTicket"];
            dataColumn.DataSource = ctx.TypesTickets.ToList();
            dataColumn.DisplayMember = "NameType";
            dataColumn.ValueMember = "Id";
            dataColumn.DataPropertyName = "IdTypeTicket";

            if (idDirection != 0)
                d = ctx.Directions.Single(x => x.Id == idDirection);
            else
            {
                d = new Directions
                {
                    DateArrival = DateTime.Now,
                    DateDeparture = DateTime.Now
                };
                ctx.Directions.Add(d);
            }

            ctx.Tickets.Where(x => x.IdDirection == d.Id).Load();
            gridViewTickets.DataSource = ctx.Tickets.Local.ToBindingList();

            tbNumberСruise.DataBindings.Add(new Binding("Text", d, "NumberСruise", true, DataSourceUpdateMode.OnPropertyChanged));
            cbTransports.DataBindings.Add(new Binding("SelectedValue", d, "IdTransport", true, DataSourceUpdateMode.OnPropertyChanged));
            cbCitiesFrom.DataBindings.Add(new Binding("SelectedValue", d, "Arrival", true, DataSourceUpdateMode.OnPropertyChanged));
            cbCitiesTo.DataBindings.Add(new Binding("SelectedValue", d, "Departure", true, DataSourceUpdateMode.OnPropertyChanged));
            timePickerDeparture.DataBindings.Add(new Binding("Value", d, "DateDeparture", true, DataSourceUpdateMode.OnPropertyChanged));
            timePickerArrival.DataBindings.Add(new Binding("Value", d, "DateArrival", true, DataSourceUpdateMode.OnPropertyChanged));
            gridViewTickets.DataError += (sen, ev) => { };
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            gridViewTickets.EndEdit();

            if (idDirection == 0 || checkBoxReset.Checked)
                d.CountAvailable = ctx.Transports.Single(x => x.Id == (int)cbTransports.SelectedValue).NumberOfSeats;

            ctx.SaveChanges();
            Close();
        }

        private void gridViewTickets_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e) => e.Row.Cells["IdDirection"].Value = d.Id;
    }
}
