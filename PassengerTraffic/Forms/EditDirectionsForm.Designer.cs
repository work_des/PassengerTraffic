﻿namespace PassengerTraffic.Forms
{
    partial class EditDirectionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbCitiesFrom = new System.Windows.Forms.ComboBox();
            this.cbCitiesTo = new System.Windows.Forms.ComboBox();
            this.cbTransports = new System.Windows.Forms.ComboBox();
            this.checkBoxReset = new System.Windows.Forms.CheckBox();
            this.timePickerDeparture = new System.Windows.Forms.DateTimePicker();
            this.timePickerArrival = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.gridViewTickets = new System.Windows.Forms.DataGridView();
            this.IdTypeTicket = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.IdDirection = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label7 = new System.Windows.Forms.Label();
            this.tbNumberСruise = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTickets)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(0, 263);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(545, 26);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 219);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Сбросить это направление:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Куда:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "От куда:";
            // 
            // cbCitiesFrom
            // 
            this.cbCitiesFrom.DisplayMember = "Name";
            this.cbCitiesFrom.FormattingEnabled = true;
            this.cbCitiesFrom.Location = new System.Drawing.Point(90, 36);
            this.cbCitiesFrom.Name = "cbCitiesFrom";
            this.cbCitiesFrom.Size = new System.Drawing.Size(139, 21);
            this.cbCitiesFrom.TabIndex = 13;
            this.cbCitiesFrom.ValueMember = "id";
            // 
            // cbCitiesTo
            // 
            this.cbCitiesTo.DisplayMember = "Name";
            this.cbCitiesTo.FormattingEnabled = true;
            this.cbCitiesTo.Location = new System.Drawing.Point(90, 75);
            this.cbCitiesTo.Name = "cbCitiesTo";
            this.cbCitiesTo.Size = new System.Drawing.Size(139, 21);
            this.cbCitiesTo.TabIndex = 14;
            this.cbCitiesTo.ValueMember = "id";
            // 
            // cbTransports
            // 
            this.cbTransports.DisplayMember = "NameAuto";
            this.cbTransports.FormattingEnabled = true;
            this.cbTransports.Location = new System.Drawing.Point(90, 112);
            this.cbTransports.Name = "cbTransports";
            this.cbTransports.Size = new System.Drawing.Size(139, 21);
            this.cbTransports.TabIndex = 15;
            this.cbTransports.ValueMember = "Id";
            // 
            // checkBoxReset
            // 
            this.checkBoxReset.AutoSize = true;
            this.checkBoxReset.Location = new System.Drawing.Point(163, 219);
            this.checkBoxReset.Name = "checkBoxReset";
            this.checkBoxReset.Size = new System.Drawing.Size(15, 14);
            this.checkBoxReset.TabIndex = 16;
            this.checkBoxReset.UseVisualStyleBackColor = true;
            // 
            // timePickerDeparture
            // 
            this.timePickerDeparture.CustomFormat = "dd.MM.yyyy HH:mm";
            this.timePickerDeparture.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timePickerDeparture.Location = new System.Drawing.Point(91, 147);
            this.timePickerDeparture.Name = "timePickerDeparture";
            this.timePickerDeparture.Size = new System.Drawing.Size(139, 20);
            this.timePickerDeparture.TabIndex = 17;
            // 
            // timePickerArrival
            // 
            this.timePickerArrival.CustomFormat = "dd.MM.yyyy HH:mm";
            this.timePickerArrival.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timePickerArrival.Location = new System.Drawing.Point(91, 183);
            this.timePickerArrival.Name = "timePickerArrival";
            this.timePickerArrival.Size = new System.Drawing.Size(139, 20);
            this.timePickerArrival.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 26);
            this.label3.TabIndex = 19;
            this.label3.Text = "Дата\r\nотправления:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Транспорт:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 177);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 26);
            this.label6.TabIndex = 21;
            this.label6.Text = "Дата\r\nприбытия:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // gridViewTickets
            // 
            this.gridViewTickets.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridViewTickets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridViewTickets.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdTypeTicket,
            this.IdDirection,
            this.Price});
            this.gridViewTickets.Location = new System.Drawing.Point(235, 9);
            this.gridViewTickets.Name = "gridViewTickets";
            this.gridViewTickets.Size = new System.Drawing.Size(298, 196);
            this.gridViewTickets.TabIndex = 22;
            this.gridViewTickets.DefaultValuesNeeded += new System.Windows.Forms.DataGridViewRowEventHandler(this.gridViewTickets_DefaultValuesNeeded);
            // 
            // IdTypeTicket
            // 
            this.IdTypeTicket.DataPropertyName = "IdTypeTicket";
            this.IdTypeTicket.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.IdTypeTicket.HeaderText = "Тип";
            this.IdTypeTicket.Name = "IdTypeTicket";
            this.IdTypeTicket.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IdTypeTicket.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.IdTypeTicket.Width = 150;
            // 
            // IdDirection
            // 
            this.IdDirection.DataPropertyName = "IdDirection";
            this.IdDirection.HeaderText = "IdDirection";
            this.IdDirection.Name = "IdDirection";
            this.IdDirection.Visible = false;
            // 
            // Price
            // 
            this.Price.DataPropertyName = "Price";
            this.Price.HeaderText = "Стоимость";
            this.Price.Name = "Price";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Номер рейса:";
            // 
            // tbNumberСruise
            // 
            this.tbNumberСruise.Location = new System.Drawing.Point(91, 6);
            this.tbNumberСruise.Name = "tbNumberСruise";
            this.tbNumberСruise.Size = new System.Drawing.Size(138, 20);
            this.tbNumberСruise.TabIndex = 24;
            // 
            // EditDirectionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(545, 289);
            this.Controls.Add(this.tbNumberСruise);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.gridViewTickets);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.timePickerArrival);
            this.Controls.Add(this.timePickerDeparture);
            this.Controls.Add(this.checkBoxReset);
            this.Controls.Add(this.cbTransports);
            this.Controls.Add(this.cbCitiesTo);
            this.Controls.Add(this.cbCitiesFrom);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSave);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditDirectionsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Изменение направления";
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTickets)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbCitiesFrom;
        private System.Windows.Forms.ComboBox cbCitiesTo;
        private System.Windows.Forms.ComboBox cbTransports;
        private System.Windows.Forms.CheckBox checkBoxReset;
        private System.Windows.Forms.DateTimePicker timePickerDeparture;
        private System.Windows.Forms.DateTimePicker timePickerArrival;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView gridViewTickets;
        private System.Windows.Forms.DataGridViewComboBoxColumn IdTypeTicket;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdDirection;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbNumberСruise;
    }
}