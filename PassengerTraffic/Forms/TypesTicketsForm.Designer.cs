﻿namespace PassengerTraffic.Forms
{
    partial class TypesTicketsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridViewTypesTickets = new System.Windows.Forms.DataGridView();
            this.NameType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTypesTickets)).BeginInit();
            this.SuspendLayout();
            // 
            // gridViewTypesTickets
            // 
            this.gridViewTypesTickets.AllowUserToOrderColumns = true;
            this.gridViewTypesTickets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridViewTypesTickets.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameType});
            this.gridViewTypesTickets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridViewTypesTickets.Location = new System.Drawing.Point(0, 0);
            this.gridViewTypesTickets.Name = "gridViewTypesTickets";
            this.gridViewTypesTickets.Size = new System.Drawing.Size(346, 240);
            this.gridViewTypesTickets.TabIndex = 0;
            // 
            // NameType
            // 
            this.NameType.DataPropertyName = "NameType";
            this.NameType.HeaderText = "Наименование";
            this.NameType.Name = "NameType";
            this.NameType.Width = 300;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSave.Location = new System.Drawing.Point(0, 217);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(346, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // TypesTicketsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 240);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.gridViewTypesTickets);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TypesTicketsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Типы билетов";
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTypesTickets)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridViewTypesTickets;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameType;
        private System.Windows.Forms.Button btnSave;
    }
}